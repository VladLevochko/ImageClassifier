import os
from PIL import Image


class Controller:

    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.view.set_controller(self)
        self.images = []
        self.current_image_index = 0

    def run(self, image_folder=None):
        print("[.] running application in ", image_folder)

        if image_folder is None or not os.path.isdir(image_folder):
            self.process_incorrect_directory()
            return

        self.images.clear()
        print("[-] looking for images")
        for item in os.listdir(image_folder):
            item_path = os.path.join(image_folder, item)
            if os.path.isfile(item_path) and self.is_image(item):
                print("[.] found image", item)

                self.images.append(Image.open(item_path))

        self.current_image_index = -1
        self.process_correct_directory(image_folder)
        self.process_next()

    def process_incorrect_directory(self):
        message = "Empty or incorrect directory!"
        self.view.show_state(message)

    def process_correct_directory(self, directory):
        message = "Images from : " + directory
        self.view.show_state(message)

    def process_next(self):
        self.current_image_index += 1
        if self.current_image_index == len(self.images):
            self.current_image_index = 0

        self.process_image()

    def process_previous(self):
        self.current_image_index -= 1
        if self.current_image_index < 0:
            self.current_image_index = len(self.images) - 1

        self.process_image()

    def process_image(self):
        print("[.] processing image", self.images[self.current_image_index].filename)

        image = self.images[self.current_image_index]

        self.view.show_image(image)
        predictions = self.model.classify(image)
        self.view.show_predictions(predictions)

    def is_image(self, file_name):
        parts = file_name.split(".")

        # return parts[-1] == "png" or parts[-1] == "jpg"
        return parts[-1] == "jpg"
