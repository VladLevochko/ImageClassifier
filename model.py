import os

import tensorflow as tf
import tensorflow.contrib.slim as slim

from nets import inception_v3 as inception
from preprocessing import inception_preprocessing
from datasets import imagenet

from PIL import Image
import numpy as np
import time


relative_path = os.path.split(os.path.realpath(__file__))[0]
CHECKPOINT_PATH = os.path.join(relative_path, "checkpoints")


class Classifier:
    def __init__(self, checkpoint_path=CHECKPOINT_PATH):
        self.image_size = inception.inception_v3.default_image_size
        self.names = imagenet.create_readable_names_for_imagenet_labels()
        self.checkpoint_path = checkpoint_path
        self.session = tf.Session()

        self.im_ph = tf.placeholder(tf.uint8, (self.image_size, self.image_size, 3))
        self.processed_image = inception_preprocessing.preprocess_image(self.im_ph,
                                                                        self.image_size, self.image_size,
                                                                        is_training=False)
        processed_images = tf.expand_dims(self.processed_image, 0)

        with slim.arg_scope(inception.inception_v3_arg_scope()):
            logits, _ = inception.inception_v3(processed_images, num_classes=1001, is_training=False)

        self.probabilities = tf.nn.softmax(logits)

        init_fn = slim.assign_from_checkpoint_fn(
            os.path.join(self.checkpoint_path, 'inception_v3.ckpt'),
            slim.get_variables_to_restore())
        init_fn(self.session)

    def classify(self, image, predictions_num=5, verbose=False):
        if verbose:
            print("[.] Classifying image", image.filename)

        image = image.resize((self.image_size, self.image_size))
        image = np.array(image)

        np_image, probabilities = self.session.run([self.processed_image, self.probabilities],
                                                   feed_dict={self.im_ph: image})
        probabilities = probabilities[0, 0:]
        sorted_inds = [i[0] for i in sorted(enumerate(-probabilities), key=lambda x: x[1])]

        prediction = []

        for i in range(predictions_num):
            index = sorted_inds[i]
            prediction.append((self.names[index], probabilities[index]))

            if verbose:
                print('Probability %0.2f%% => [%s]' % (100 * probabilities[index], self.names[index]))

        if verbose:
            print("[.] Done")

        return prediction

    def close(self):
        self.session.close()


if __name__ == "__main__":
    classifier = Classifier("checkpoints")

    image = Image.open("/home/vladyslav/Pictures/2dd40315cac95f9bcce234082b1938a6.jpeg")
    start = time.time()
    classifier.classify(image, 5, True)
    end = time.time()
    print("execution time", end - start)

    image = Image.open("/home/vladyslav/Pictures/ZIMBABWE1.jpg")
    start = time.time()
    classifier.classify(image, 3, True)
    end = time.time()
    print("execution time", end - start)

    image = Image.open("/home/vladyslav/Pictures/567332612083df4272af30ba.jpg")
    start = time.time()
    classifier.classify(image, 3, True)
    end = time.time()
    print("execution time", end - start)

    classifier.close()

