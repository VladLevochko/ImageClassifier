import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QHBoxLayout, QGridLayout, QLineEdit, QPushButton, \
    QFormLayout, QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QFont
from PIL.ImageQt import ImageQt


class QtView(QWidget):
    def __init__(self, title="Image Classifier"):
        super().__init__()
        self.title = title
        self.top = 0
        self.left = 0
        # self.width = 1000
        # self.height = 500
        self.controller = None

        self.layout = None
        self.image_label = None
        self.grid = None
        self.grid_labels = []
        self.state_label = None
        self.path_input = None
        self.process_button = None
        self.next_button = None
        self.prev_button = None

        self.max_image_width = 500
        self.max_image_height = 500

        self.font = QFont("Arial", 20, QFont.Bold)

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle(self.title)

        self.layout = QHBoxLayout()
        self.layout.addWidget(self.get_image_widget())
        self.layout.addWidget(self.get_right_side_widget())
        self.setLayout(self.layout)

        self.show()

    def get_image_widget(self):
        image_wrapper = QWidget()
        wrapper_layout = QVBoxLayout()
        self.image_label = QLabel()
        wrapper_layout.addWidget(self.image_label)
        image_wrapper.setLayout(wrapper_layout)

        return image_wrapper

    def get_right_side_widget(self):
        right_side = QWidget()

        layout = QVBoxLayout()
        layout.addWidget(self.get_predictions_widget())
        layout.addWidget(self.get_control_widget())

        right_side.setLayout(layout)

        return right_side

    def get_predictions_widget(self):
        self.grid = QWidget()
        grid = QGridLayout()
        for i in range(1, 11):
            self.grid_labels.append([])
            for j in range(1, 3):
                label = QLabel()
                label.setFont(self.font)

                grid.addWidget(label, i, j)
                self.grid_labels[i - 1].append(label)

        self.grid.setLayout(grid)

        return self.grid

    def get_control_widget(self):
        controll = QWidget()
        form_layout = QFormLayout()

        self.state_label = QLabel("state label")
        path_input_label = QLabel("Images")
        self.path_input = QLineEdit()
        self.process_button = QPushButton("Process")
        self.prev_button = QPushButton("Previous")
        self.next_button = QPushButton("Next")

        form_layout.addRow(QWidget(), self.state_label)
        form_layout.addRow(path_input_label, self.path_input)
        form_layout.addRow(QWidget(), self.process_button)
        form_layout.addRow(self.prev_button, self.next_button)

        controll.setLayout(form_layout)

        return controll

    def set_controller(self, controller):
        self.controller = controller

        self.process_button.clicked.connect(lambda: self.controller.run(self.path_input.text()))
        self.prev_button.clicked.connect(self.controller.process_previous)
        self.next_button.clicked.connect(self.controller.process_next)

    def show_state(self, state_message):
        self.state_label.setText(state_message)

    def show_image(self, image):
        image = self.scale_image(image)
        qimage = ImageQt(image)
        qpixmap = QPixmap.fromImage(qimage)
        self.image_label.setPixmap(qpixmap)

    def scale_image(self, image):
        if image.width > image.height:
            ratio = image.width / self.max_image_width
            image = image.resize((self.max_image_width, int(image.height / ratio)))
        else:
            ratio = image.height / self.max_image_height
            image = image.resize((int(image.width / ratio), self.max_image_height))

        return image

    def show_predictions(self, predictions):
        for i in range(len(predictions)):
            labels, probability = predictions[i]

            self.grid_labels[i][0].setText(labels.split(",")[0].capitalize())
            self.grid_labels[i][1].setText("{:.2%}".format(probability))

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Left:
            self.controller.process_previous()
        if e.key() == Qt.Key_Right or e.key() == Qt.Key_Space:
            self.controller.process_next()
        if e.key() == Qt.Key_Escape:
            self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = QtView('Hello world')
    sys.exit(app.exec_())
