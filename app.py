import sys
from PyQt5.QtWidgets import QApplication
from model import Classifier
from view import QtView
from controller import Controller


if __name__ == "__main__":

    app = QApplication(sys.argv)

    model = Classifier()
    view = QtView()
    controller = Controller(model, view)

    controller.run()

    sys.exit(app.exec_())
